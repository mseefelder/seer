#include "minimage-deinterlaced.hpp"
#include <iostream>
#include <Eigen/Dense>

template<typename T>
void resampleWithMatrix(
    Image<T>& inImg, 
    Eigen::Matrix3f& m,
    float roiWidth, float roiHeight,
    std::size_t outWidth, std::size_t outHeight,
    Image<T>& outImg
){
    float stepX = roiWidth/outWidth;
    float stepY = roiHeight/outHeight;

    Eigen::Vector3f samplePos;
    T* pixel = new T[inImg.getChannels()];
    for (std::size_t x = 0; x<outWidth; x++) {
        for (std::size_t y = 0; y<outHeight; y++) {
            samplePos << x*stepX, y*stepY, 1.0;
            samplePos = (m*samplePos);
            samplePos = samplePos/samplePos(2);
            inImg.nearRead(samplePos(0), samplePos(1), pixel);
            outImg.write(x,y,pixel);
        }
    }
    delete [] pixel;
}

template<typename T>
void resampleWithMatrix(
    Image<T>& inImg, 
    Eigen::Matrix3f& m,
    float lowerLeftX, float lowerLeftY,
    float upperRightX, float upperRightY,
    std::size_t outWidth, std::size_t outHeight,
    Image<T>& outImg
){
    //std::cout<<"llX: "<<lowerLeftX<<" llY: "<<lowerLeftY<<
    //    "\nurX: "<<upperRightX<<" urY: "<<upperRightY<<
    //    "\noutWidth: "<<outWidth<<" outHeight: "<<outHeight<<
    //    std::endl;

    float stepX = (upperRightX-lowerLeftX)/outWidth;
    float stepY = (upperRightY-lowerLeftY)/outHeight;

    Eigen::Vector3f samplePos;
    T* pixel = new T[inImg.getChannels()];
    for (std::size_t x = 0; x<outWidth; x++) {
        for (std::size_t y = 0; y<outHeight; y++) {
            samplePos << lowerLeftX+x*stepX, lowerLeftY+y*stepY, 1.0;
            samplePos = (m*samplePos);
            samplePos = samplePos/samplePos(2);
            inImg.nearRead(samplePos(0), samplePos(1), pixel);
            outImg.write(x,y,pixel);
        }
    }
    delete [] pixel;
}

template<typename T>
void transformWholeImage (
    Image<T>& inImg,
    Eigen::Matrix3f &M,
    std::size_t outWidth,
    Image<T>& outImg){

    Eigen::Matrix3f MI = M.inverse().eval();

    Eigen::Matrix<float, 3, 4> cornerMatrix;
    cornerMatrix <<
        0, 0,                 inImg.getWidth(),  inImg.getWidth(),
        0, inImg.getHeight(), inImg.getHeight(), 0,
        1, 1,                 1,                 1
    ;
    
    //std::cout<<cornerMatrix<<std::endl;

    cornerMatrix = M*cornerMatrix;

    //std::cout<<cornerMatrix<<std::endl;

    cornerMatrix.col(0) = (1/cornerMatrix(2,0)) * cornerMatrix.col(0);
    cornerMatrix.col(1) = (1/cornerMatrix(2,1)) * cornerMatrix.col(1);
    cornerMatrix.col(2) = (1/cornerMatrix(2,2)) * cornerMatrix.col(2);
    cornerMatrix.col(3) = (1/cornerMatrix(2,3)) * cornerMatrix.col(3);

    //std::cout<<cornerMatrix<<std::endl;

    Eigen::Vector3f maxVals = cornerMatrix.rowwise().maxCoeff();

    //std::cout<<maxVals.transpose()<<std::endl;

    Eigen::Vector3f minVals = cornerMatrix.rowwise().minCoeff();
    
    //std::cout<<minVals.transpose()<<std::endl;

    float ratio = (maxVals(0)-minVals(0))/(maxVals(1)-minVals(1));

    std::size_t outHeight = ratio*outWidth;
    outImg.resize(outWidth, outHeight, inImg.getChannels());
    resampleWithMatrix<T> (inImg, MI, minVals(0), minVals(1), maxVals(0), maxVals(1), outWidth, outHeight, outImg);
}

template<typename T>
void rectify(
    Image<T>& inImg, 
    int* pts,
    float roiWidth, float roiHeight,
    std::size_t outWidth,
    Image<T>& outImg) {

        std::size_t outHeight = (outWidth/roiWidth)*roiHeight;
        outImg.resize(outWidth, outHeight, inImg.getChannels());

        Eigen::Matrix<float, 8, 1> world;
        world << 
            0.0, 0.0,
            0.0, roiHeight,
            roiWidth, roiHeight,
            roiWidth, 0.0
        ; 

        Eigen::Matrix<float, 8, 8> pointMatrix;
        pointMatrix <<
        pts[0], pts[1],   1.0,   0.0,   0.0,   0.0, -pts[0]*world(0), -pts[1]*world(0),
          0.0,   0.0,   0.0, pts[0], pts[1],   1.0, -pts[0]*world(1), -pts[1]*world(1),
        pts[2], pts[3],   1.0,   0.0,   0.0,   0.0, -pts[2]*world(2), -pts[3]*world(2),
          0.0,   0.0,   0.0, pts[2], pts[3],   1.0, -pts[2]*world(3), -pts[3]*world(3),
        pts[4], pts[5],   1.0,   0.0,   0.0,   0.0, -pts[4]*world(4), -pts[5]*world(4),
          0.0,   0.0,   0.0, pts[4], pts[5],   1.0, -pts[4]*world(5), -pts[5]*world(5),
        pts[6], pts[7],   1.0,   0.0,   0.0,   0.0, -pts[6]*world(6), -pts[7]*world(6),
          0.0,   0.0,   0.0, pts[6], pts[7],   1.0, -pts[6]*world(7), -pts[7]*world(7)
        ;

        
        Eigen::Matrix<float, 8, 1> coeff = pointMatrix.colPivHouseholderQr().solve(world);
        
        Eigen::Matrix3f world2screen; 
        world2screen <<
        coeff(0), coeff(1), coeff(2),
        coeff(3), coeff(4), coeff(5),
        coeff(6), coeff(7),      1.0
        ;
        world2screen = world2screen.inverse().eval();
        
        resampleWithMatrix<T> (inImg, world2screen, roiWidth, roiHeight, outWidth, outHeight, outImg);
}

template<typename T>
void rectifyAffine(
    Image<T>& inImg, 
    int* pts,
    std::size_t outWidth,
    Image<T>& outImg) {
    
    Eigen::Vector3f point[8];
    Eigen::Vector3f line[4];
    Eigen::Vector3f infPoint[2];
    Eigen::Vector3f infLine;
    Eigen::Matrix3f Hp = Eigen::Matrix3f::Identity();
    Eigen::Matrix3f HpIt = Eigen::Matrix3f::Identity();
    for(int i = 0; i<8; i++) {
        point[i] = Eigen::Vector3f(pts[2*i], pts[1+2*i], 1.0);
    }
    
    //std::cout<<"Points set"<<std::endl;

    for(int i = 0; i<4; i++) {
        line[i] = point[2*i].cross(point[1+2*i]);
    }
    
    //std::cout<<"Calculated lines"<<std::endl;

    for(int i = 0; i<2; i++) {
        infPoint[i] = line[2*i].cross(line[1+2*i]);
    }
    
    //std::cout<<"Calculated infPoints"<<std::endl;

    infLine = infPoint[0].cross(infPoint[1]);
    
    //std::cout<<infLine.transpose()<<std::endl;

    Hp.row(2) = infLine;
    
    //std::cout<<Hp<<std::endl;

    transformWholeImage<T>(inImg, Hp, outWidth, outImg);

    /*
    std::cout<<"DEBUG:\n";
    Eigen::Vector3f dbg = HpIt*Eigen::Vector3f(minVals(0),minVals(1),minVals(2));
    dbg = dbg/dbg(2);
    std::cout<<dbg.transpose()<<"\n";
    dbg = HpIt*Eigen::Vector3f(maxVals(0),maxVals(1),maxVals(2));
    dbg = dbg/dbg(2);
    std::cout<<dbg.transpose()<<std::endl;
    */
}

template<typename T>
void affineToSimilarity(
    Image<T>& inImg, 
    int* pts,
    std::size_t outWidth,
    Image<T>& outImg) {

    Eigen::Vector3f point[8];
    Eigen::Vector3f line[4];
    Eigen::Matrix2f A;
    Eigen::Matrix3f H;

    for(int i = 0; i<8; i++) {
        point[i] = Eigen::Vector3f(pts[2*i], pts[1+2*i], 1.0);
    }
    
    //std::cout<<"Points set"<<std::endl;

    for(int i = 0; i<4; i++) {
        line[i] = point[2*i].cross(point[1+2*i]);
    }
    
    //std::cout<<"Calculated lines"<<std::endl;

    A << 
    line[0](0)*line[1](0), line[0](0)*line[1](1) + line[0](1)*line[1](0),
    line[2](0)*line[3](0), line[2](0)*line[3](1) + line[2](1)*line[3](0);

    Eigen::Vector2f b(-line[0](1)*line[1](1), -line[2](1)*line[3](1));

    Eigen::Matrix<float, 2, 1> x = A.colPivHouseholderQr().solve(b);

    H << 
    x(0), x(1), 0,
    x(1), 1.0,  0,
    0,      0,  0;

    transformWholeImage<T>(inImg, H, outWidth, outImg);

}