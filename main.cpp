#include "rectify.hpp"
#include "CImg.h"
#include <iostream>
#include <string>

using namespace cimg_library;
#ifndef cimg_imagepath
#define cimg_imagepath "img/"
#endif

int main(int argc, char* argv[]) {
    const char* file_i = cimg_option("-i", cimg_imagepath, "Input image");
    const int mode = cimg_option("-m", 0, "Application mode: 0 - ROI selection; 1 - Infinity line");
    const CImg<float> img(file_i);
    CImg<float> dispimg(file_i);
    
    Image<float> srcImg;
    Image<float> outImg;
    Image<float> outImg2;
    srcImg.set(img.data(), img.width(), img.height(), img.spectrum());

    switch(mode) {
        case 0: {
            CImgDisplay main_disp(dispimg,"Click 4 points");
            CImgDisplay res_disp(dispimg,"Result");
            static const unsigned char pointColor[3] = {255, 0, 0};
            int pointcount = 0;
            int points[8];
            float worldWidth, worldHeight;
            while (!main_disp.is_closed()) {
                for(int i = 0; i<pointcount; i++) {
                    dispimg.draw_circle(points[i*2],points[i*2+1],3,pointColor);
                }
                dispimg.display(main_disp);
                main_disp.wait();
                if (main_disp.button() && main_disp.mouse_y()>=0 && main_disp.mouse_x()>=0) {
                    if(pointcount<4) {
                        points[pointcount*2] = main_disp.mouse_x() ;
                        points[pointcount*2+1] = main_disp.mouse_y();
                        pointcount++;
                        main_disp.set_title(("Click "+std::to_string(4-pointcount)+" points").c_str());
                    }
                    else {
                        main_disp.set_title("Input values in console");
                        std::cout<<"Enter real world width of ROI:\n"<<std::endl;
                        std::cin>>worldWidth;
                        std::cout<<"Enter real world height of ROI:\n"<<std::endl;
                        std::cin>>worldHeight;
            
                        rectify<float>(srcImg, points, worldWidth, worldHeight, srcImg.getWidth(), outImg);
                        CImg<float> final(outImg.getData(), outImg.getWidth(), outImg.getHeight(), 1, outImg.getChannels());
                        final.save_png("result.png");
                        final.display(res_disp);
                    }
                }
            }
            break;
        } 
        case 1: {
            CImgDisplay main_disp(dispimg,"Click 8 points");
            CImgDisplay res_disp(dispimg,"Result");
            static const unsigned char pointColor[3] = {255, 0, 0};
            int pointcount = 0;
            int points[16];
            float worldWidth, worldHeight;
            while (!main_disp.is_closed()) {
                for(int i = 0; i<pointcount; i++) {
                    dispimg.draw_circle(points[i*2],points[i*2+1],3,pointColor);
                }
                dispimg.display(main_disp);
                main_disp.wait();
                if (main_disp.button() && main_disp.mouse_y()>=0 && main_disp.mouse_x()>=0) {
                    if(pointcount<8) {
                        points[pointcount*2] = main_disp.mouse_x() ;
                        points[pointcount*2+1] = main_disp.mouse_y();
                        pointcount++;
                        main_disp.set_title(("Click "+std::to_string(8-pointcount)+" points").c_str());
                    }
                    else {    
                        rectifyAffine<float>(srcImg, points, srcImg.getWidth(), outImg);
                        CImg<float> final(outImg.getData(), outImg.getWidth(), outImg.getHeight(), 1, outImg.getChannels());
                        final.save_png("result.png");
                        final.display(res_disp);
                    }
                }
            }
            break;
        }
        case 2: {
            CImgDisplay main_disp(dispimg,"Click 8 points");
            CImgDisplay res_disp(dispimg,"Result");
            static const unsigned char pointColor[3] = {255, 0, 0};
            int pointcount = 0;
            int points[16];
            bool perspectiveRemoved = false;
            float worldWidth, worldHeight;
            while (!main_disp.is_closed()) {
                //std::cout<<"Loop begin\n";
                dispimg.display(main_disp);
                //std::cout<<"Displayed. Waiting...\n";
                main_disp.wait();
                //std::cout<<"Event!\n";
                if (main_disp.button() && main_disp.mouse_y()>=0 && main_disp.mouse_x()>=0) {
                    std::cout<<"Valid click!\n";
                    if(pointcount<8) {
                        std::cout<<"Setting point "<<pointcount<<"\n";
                        points[pointcount*2] = main_disp.mouse_x() ;
                        points[pointcount*2+1] = main_disp.mouse_y();
                        pointcount++;
                        main_disp.set_title(("Click "+std::to_string(8-pointcount)+" points").c_str());
                        dispimg.draw_circle(main_disp.mouse_x(),main_disp.mouse_y(),3,pointColor);
                        std::cout<<"Point set!\n";
                    }
                    else {    
                        std::cout<<"All points selected: "<<pointcount<<"\n";
                        if(!perspectiveRemoved){
                            std::cout<<"Removing perspective...\n";
                            rectifyAffine<float>(srcImg, points, srcImg.getWidth(), outImg);
                            dispimg.clear();
                            dispimg.assign(outImg.getData(), outImg.getWidth(), outImg.getHeight(), 1, outImg.getChannels());
                            perspectiveRemoved = true;
                            pointcount = 0;
                            std::cout<<"Perspective removed!\n";
                        }
                        else {
                            std::cout<<"Affine -> similarity...\n";
                            affineToSimilarity<float>(outImg, points, srcImg.getWidth() ,outImg2);
                            CImg<float> final(outImg2.getData(), outImg2.getWidth(), outImg2.getHeight(), 1, outImg2.getChannels());
                            final.save_png("result.png");
                            final.display(res_disp);
                            std::cout<<"All done\n";
                        }
                    }
                    std::cout<<"Loop end!"<<std::endl;
                }
            }
            break;
        }    
        default: {
            std::cout<<"Invalid mode!"<<std::endl;
            break;
        }
    }

    return 0;
}