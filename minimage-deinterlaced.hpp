#include <cstring>

template<typename T> 
class Image {
private:
    T* data = NULL;
    std::size_t width, height, channels, pixels;
    int counter;
    
    std::size_t index(std::size_t x, std::size_t y, std::size_t c) {
        return x+y*width+c*pixels;
    }

    bool checkBounds(std::size_t x, std::size_t y) {
        return (x>=0)&&(x<width)&&(y>=0)&&(y<height);
    }

public:
    Image() {
        data = NULL;
        width = 0;
        height = 0;
        channels = 0;
        pixels = 0;
        counter = 0;
    }
    
    Image(std::size_t w, std::size_t h, std::size_t N) : width(w), height(h), channels(N), counter(0) {
        pixels = width*height;
        data = new T[channels*width*height];
    }

    ~Image() {
        delete [] data;
    }

    bool set(const T* src, std::size_t w, std::size_t h, std::size_t N) {
        resize(w, h, N);
        std::memcpy(data, src, w*h*N*sizeof(T));
    }

    bool read(std::size_t x, std::size_t y, T* dest) {
        if(checkBounds(x,y)) {
            for(counter = 0; counter < channels; counter++){
                dest[counter] = data[index(x,y,counter)];
            }
            return true;
        }
        return false;
    }

    bool nearRead(float x, float y, T* dest) {
        std::size_t sx, sy;
        sx = (std::size_t) x;
        sy = (std::size_t) y;
        return read(sx, sy, dest);
    }

    bool write(std::size_t x, std::size_t y, T* source) {
        if(checkBounds(x,y)) {
            for(counter = 0; counter < channels; counter++){
                data[index(x,y,counter)] = source[counter];
            }
            return true;
        }
        return false;
    }

    bool resize(std::size_t w, std::size_t h, std::size_t N) {
        if(data) {
            delete [] data;
        }
        width = w;
        height = h;
        pixels = width*height;
        channels = N;
        data = new T[channels*width*height];
    }

    T* getData() {
        return data;
    }

    std::size_t getWidth() {
        return width;
    }
    
    std::size_t getHeight() {
        return height;
    }

    std::size_t getChannels() {
        return channels;
    }
};